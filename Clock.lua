Clock = {}

Clock.hour = 0
Clock.min = 0
Clock.sec = 0
Clock.lastUpdate = nil

function Clock.Initialize()

   CreateWindow("ClockWindow", true)

   LayoutEditor.RegisterWindow( "ClockWindow",
                                L"Clock",
                                L"Displays the current local time.",
                                false, false,
                                true, nil )
    Clock.lastUpdate = nil
    
    if not ClockSettings then
        ClockSettings = {
            Hours = 24,
            AmPm = false,
            Format = L"%2d:%02d:%02d",
            R = 255,
            G = 255,
            B = 255,
        }
    end
end

function Clock.OnUpdate(elapsedTime)

    local comptime = GetComputerTime()
    if comptime == Clock.lastUpdate then return end
    Clock.lastUpdate = comptime
    
    local secs = comptime % 60
    comptime = (comptime - secs) / 60
    local mins = comptime % 60
    comptime = (comptime - mins) / 60
    local hours = comptime
    
    Clock.sec,Clock.min,Clock.hour = secs,mins,hours
    
    local fmt = ClockSettings.Format
    if ClockSettings.AmPm then
        if Clock.hour < 12 then fmt=fmt..L" AM" else fmt=fmt..L" PM" end
    end
    
    if (Clock.hour == 0 or Clock.hour == 12) and ClockSettings.Hours == 12 then
        LabelSetText("ClockWindowText", wstring.format(fmt, 12, Clock.min, Clock.sec))
    else
        LabelSetText("ClockWindowText", wstring.format(fmt, Clock.hour % ClockSettings.Hours, Clock.min, Clock.sec))
    end
    LabelSetTextColor("ClockWindowText", ClockSettings.R, ClockSettings.G, ClockSettings.B)
end